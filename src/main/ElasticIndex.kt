/*
 * cluster update transformation
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.Klaxon
import org.apache.http.HttpHost
import org.apache.kafka.streams.KeyValue
import org.apache.logging.log4j.Logger
import org.elasticsearch.ElasticsearchException
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest
import org.elasticsearch.action.get.GetRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.cluster.health.ClusterHealthStatus
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*
import kotlin.system.exitProcess

class ElasticIndex(private val clusterBuilder: IdClusterBuilder,
                   private val parser: JsonParser,
                   private val properties: Properties,
                   private val log: Logger) {


    private val elastic = connect()

    private fun connect(): RestHighLevelClient {
        val hosts = properties.getProperty("elastic.hosts").split(',')
        val port = properties.getProperty("elastic.port").toInt()

        val httpHosts = mutableListOf<HttpHost>()
        for (host in hosts) {
            httpHosts.add(HttpHost(host, port))
        }
        return RestHighLevelClient(
            RestClient.builder(
                *httpHosts.toTypedArray()
            )
        )
    }

    private val baseIndex = properties.getProperty("elastic.index")!!
    init {
        try {
            val response = elastic.cluster().health(ClusterHealthRequest().waitForStatus(ClusterHealthStatus.GREEN), RequestOptions.DEFAULT)

            // Accept YELLOW because of single node test environment!
            if (response.status == ClusterHealthStatus.GREEN || response.status == ClusterHealthStatus.YELLOW) {
                log.info("Cluster is ready!")
            } else {
                log.error("Cluster is not ready. Shutdown service ...")
                exitProcess(1)
            }
        } catch (ex: java.net.ConnectException) {
            log.error("Could not connect to elasticsearch at {}:{} because of {}.",
                properties.getProperty("elastic.host"),
                properties.getProperty("elastic.port"), ex.message)
            exitProcess(1)
        }
    }

    private fun searchSource(resources: Set<Resource>): SearchSourceBuilder {
        val queryBuilder = QueryBuilders
            .boolQuery()
            .minimumShouldMatch(1)
        for (resource in resources) {
            queryBuilder.should(QueryBuilders.matchQuery("identifiers.${resource.provider}", resource.identifier))
        }
        return SearchSourceBuilder()
                .query(queryBuilder)
                .seqNoAndPrimaryTerm(true)
    }

    private fun model(messageDate: String, cluster: IdCluster): SbMetadataModel {
        return  SbMetadataModel()
            .setData(Klaxon().toJsonString(cluster))
            .setEsBulkAction(EsBulkActions.CREATE)
            .setEsIndexName("$baseIndex-$messageDate")
    }

    private fun versionModel(messageDate: String, cluster: IdCluster, primaryTerm: Long, seqNo: Long): SbMetadataModel {
        return SbMetadataModel()
            .setData(Klaxon().toJsonString(cluster))
            .setEsDocumentPrimaryTerm(primaryTerm)
            .setEsDocumentSeqNum(seqNo)
            .setEsIndexName("$baseIndex-$messageDate")
            .setEsBulkAction(EsBulkActions.INDEX)
    }

    fun retry(key: String, value: Pair<String, VersionedIdCluster>): List<SbMetadataModel> {
        val response = elastic.get(GetRequest("$baseIndex-${value.first}").id(key), RequestOptions.DEFAULT)
        return if (response.isExists) {
            val existingCluster = parser.parseCluster(response.sourceAsString)[0]
            return if (existingCluster == value.second.idCluster) {
                log.warn("Retry dropped because new cluster is identical.")
                emptyList()
            } else {
                val mergedCluster = clusterBuilder.merge(existingCluster, value.second.idCluster)
                return if (mergedCluster == existingCluster) {
                    log.warn("Retry dropped because there is no change after merging cluster.")
                    emptyList()
                } else {
                    log.warn(
                        "Conflicting value merged: " +
                                "Incoming ID $key (PT:${value.second.primaryTerm}|SN:${value.second.seqNo}, " +
                                "Existing ID ${existingCluster.id} (PT:${response.primaryTerm}|SN:${response.seqNo}, "
                    )

                    listOf(versionModel(value.first, mergedCluster, response.primaryTerm, response.seqNo))
                }
            }
        } else {
            log.error("Retry  was deleted before retry: $key")
            listOf(model(value.first, value.second.idCluster))
        }
    }


    fun check(key: String, value: Pair<String, IdCluster>, update: Boolean = false): List<KeyValue<String, SbMetadataModel>> {
        val request = SearchRequest("$baseIndex-${value.first}").source(searchSource(value.second.resources))
        try {
            val response = elastic.search(request, RequestOptions.DEFAULT)
            when {
                // no hits returned means no identifiers mach any existing identifiers.
                // -> create a new value with these identifiers!
                // -> unless update = true then no new clusters are created!
                response.hits.hits.isEmpty() -> {
                    return if (update) {
                        log.info("Discarding cluster update, because no cluster is present in index for $key.")
                        emptyList()
                    } else {
                        // Keep this type annotation as it will otherwise throw a compile error...
                        listOf<KeyValue<String, SbMetadataModel>>(KeyValue(value.second.id, model(value.first, value.second)))
                    }
                }
                // a single hit was found. This means one or more identifiers are
                // in this value. The new identifiers should be merged into the old value.
                // checks if all the identifiers in the same field actually match!
                // TODO: Fix existing errors or reload?
                response.hits.hits.size == 1 -> {
                    val hit = response.hits.hits[0]!!
                    val oldCluster = Klaxon().parse<IdCluster>(hit.sourceAsString)!!
                    log.debug("Create message to be indexed with version: ${hit.primaryTerm}")
                    return if (oldCluster == value.second) {
                        log.info("Cluster is not resent as it is identical.")
                        emptyList()
                    }
                    else {
                        val mergedCluster = clusterBuilder.merge(oldCluster, value.second)
                        return if (mergedCluster == oldCluster) {
                            log.info("Merged value is dropped as it is identical to existing value.")
                            emptyList()
                        } else {
                            listOf(
                                KeyValue(
                                    hit.id,
                                    versionModel(
                                        value.first,
                                        mergedCluster,
                                        hit.primaryTerm,
                                        hit.seqNo
                                    )
                                )
                            )
                        }
                    }
                }
                // More than one hit.
                else -> {
                    log.error("message:Returned more than one value.##identifiers:$key##totalhits:${response.hits.totalHits}")
                    //logger.error("The query used: ${queryAndFields.first}.")
                    //logger.error("Total hits: ${response.hits.totalHits}")
                    return listOf()
                }
            }
        } catch (ex: ElasticsearchException) {
            log.error("exceptiontype - ElasticIndex.check:ElasticsearchException##exception:${ex.rootCause.message}")
            println("now exitProcess: exceptiontype:ElasticsearchException##exception:${ex.rootCause.message}")
            exitProcess(1)
        } catch (ex: ClusterMergeConflict) {
            println("now exitProcess - ElasticIndex.check:exceptiontype:ClusterMergeConflict##exception:Could not merge two clusters ${ex.message}.")
            log.error("exceptiontype:ClusterMergeConflict##exception:Could not merge two clusters ${ex.message}.")
        } catch (ex: java.lang.Exception) {
            val string = StringWriter()
            val print = PrintWriter(string)
            ex.printStackTrace(print)
            log.error("Unknown exception occurred: ${ex.message}")
            log.error("Stack Trace: $string")
            exitProcess(1)
        }
        return listOf()
    }

    fun update(key: String, value: Pair<String, IdCluster>): List<KeyValue<String, SbMetadataModel>> {
        return check(key, value, true)
    }


    // EXISTS ONLY FOR TESTING!!! NOT USED IN ACTUAL CODE
    fun index(key: String, value: SbMetadataModel, messageDate: String) {
        elastic.index(IndexRequest("$baseIndex-$messageDate").id(key)
            .source(value.data, XContentType.JSON)
            .setIfSeqNo(value.esDocumentSeqNum)
            .setIfPrimaryTerm(value.esDocumentPrimaryTerm), RequestOptions.DEFAULT)

    }


}