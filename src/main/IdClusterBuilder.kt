/*
 * cluster update transformation
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.logging.log4j.Logger
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit

class IdClusterBuilder(private val providers: Providers, private val log: Logger) {

    fun merge(oldCluster: IdCluster, newCluster: IdCluster): IdCluster {
        val identifiers = mutableMapOf<String, Set<String>>()
        val sequence = oldCluster.identifiers.asSequence() + newCluster.identifiers.asSequence()
        for (entry in sequence) {
            if (entry.key in identifiers.keys) {
                val value = identifiers[entry.key]
                if (value != null) {
                    identifiers[entry.key] = value union entry.value
                } else {
                    identifiers[entry.key] = entry.value
                }
            } else {
                identifiers[entry.key] = entry.value
            }
        }

        return IdCluster(
            id = oldCluster.id,
            created = if (OffsetDateTime.parse(oldCluster.created) <= OffsetDateTime.parse(newCluster.created)) oldCluster.created else newCluster.created,
            modified = if (OffsetDateTime.parse(oldCluster.created) >= OffsetDateTime.parse(newCluster.created)) oldCluster.created else newCluster.created,
            resources = oldCluster.resources union newCluster.resources,
            identifiers = identifiers

        )
    }

    fun build(links: SourceLinks): IdCluster? {
        val resources = collectResources(links)
        val identifiers = collectIdentifiers(resources)
        var gndIdentifier = ""
        var viafIdentifier = ""
        // lexical sorting the identifiers and keeping the largest. Better would be to take the
        // correct one (most additional ids are redirects!), but this would be very hard to implement and not always
        // possible.
        for (resource in resources) {
            if (resource.provider == "GND") {
                if (gndIdentifier == "")
                    gndIdentifier = resource.uri
                else {
                    val result = gndIdentifier.compareTo(resource.uri, true)
                    if (result > 0)
                        gndIdentifier = resource.uri
                }
            } else if (resource.provider == "VIAF"){
                if (viafIdentifier == "")
                    viafIdentifier = resource.uri
                else {
                    val result = viafIdentifier.compareTo(resource.uri, true)
                    if (result > 0)
                        viafIdentifier = resource.uri
                }
            }
        }

        return if (gndIdentifier == "" && viafIdentifier == "")
            null
        else
            IdCluster(
                id = if (gndIdentifier != "") gndIdentifier else viafIdentifier,
                created = OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS).toString(),
                modified = OffsetDateTime.now().truncatedTo(ChronoUnit.SECONDS).toString(),
                resources = resources,
                identifiers = identifiers
            )
    }

    private fun collectResources(message: SourceLinks): Set<Resource> {
        val result: MutableSet<String> = mutableSetOf()
        result.add(message.id)
        result.addAll(message.sameAs)

        val returnValue: MutableSet<Resource> = mutableSetOf()
        result.forEach { s ->
            try {
                val provider = providers.getProvider(s)
                val identifier = provider.getIdentifier(s)
                returnValue.add(
                    Resource(
                        provider.providerNamespace.replace("{ID}", identifier),
                        identifier,
                        provider.providerSlug
                    ))
            } catch (ex: NoProviderFoundException) {
                if (ex.isIgnored()) {
                    log.info("ignored provider: ${ex.message}")
                } else {
                    log.warn("noProviderFoundException: ${ex.message}")
                }
            } catch (ex: IdentifierExtractionFailed) {
                log.error("IdentifierExtractionFailed:  ${ex.message}")
            }
        }
        return returnValue
    }

    private fun collectIdentifiers(resources: Set<Resource>): Map<String, Set<String>> {
        val returnValue = mutableMapOf<String, MutableSet<String>>()
        resources.forEach {
            if (it.provider in returnValue.keys) {
                returnValue[it.provider]?.add(it.identifier)
            } else {
                returnValue[it.provider] = mutableSetOf(it.identifier)
            }
        }
        return returnValue
    }
}