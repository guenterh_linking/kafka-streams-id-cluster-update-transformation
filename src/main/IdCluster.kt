/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

data class IdCluster(
    val id: String,
    val created: String,
    val modified: String,
    val resources: Set<Resource>,
    val identifiers: Map<String, Set<String>>
) {
    override fun equals(other: Any?): Boolean {
        if (other == null)
            return false
        return if (other is IdCluster) {
            hashCode() == other.hashCode()
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + resources.hashCode()
        result = 31 * result + identifiers.hashCode()
        return result
    }
}