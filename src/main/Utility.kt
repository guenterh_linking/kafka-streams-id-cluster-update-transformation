/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.beust.klaxon.Klaxon
import java.io.File
import java.util.regex.Pattern

class ClusterMergeConflict(message: String) : Exception(message)

class IdentifierExtractionFailed(message: String) : Exception(message)

class NoProviderFoundException(message: String, private val uri: String) : Exception(message) {
    private val ignoredUris = listOf<Pattern>(Pattern.compile("http://d-nb.info/gnd/[0-9X]+?/about"))

    fun isIgnored(): Boolean {
        for (pattern in ignoredUris) {
            if (pattern.matcher(uri).matches())
                return true
        }
        return false
    }

}

fun loadProviders(): Providers {
    val file = File("/configsproviders/providers.json")
    return if (file.isFile)
        Klaxon().parse<Providers>(file)!!
    else
        Klaxon().parse<Providers>(ClassLoader.getSystemResourceAsStream("providers.json"))!!
}

fun combineSet(value: String, values: Set<String>?): Set<String> {
    val mut = mutableSetOf(value)
    values?.forEach {
        mut.add(it)
    }
    return mut.toSet()
}

fun combineSet(value: Set<String>, values: Set<String>?): Set<String> {
    val mut = value.toMutableSet()
    values?.forEach {
        mut.add(it)
    }
    return mut.toSet()
}