## Id Hub Merger Service

This application creates Id Hub clusters for each owl:sameAs data set put into it. Each
cluster is then checked in the Id Hub. When a cluster inside of the Hub already contains at 
least one of the identifiers present, the cluster will be retrieved and merged.

If the resulting cluster is different from the original cluster the new cluster will be sent
for indexing.

This workflow leads to a lot of versioning issues which are then reprocessed. As a result it is very slow.
This is the reason, that identical clusters are not resent, as there would be infinite loops otherwise.

This is not the nicest solution, but the best one that works.

Requires the providers.json from the shared configurations pool: https://gitlab.com/swissbib/linked/workflows/tree/master/shared-configurations